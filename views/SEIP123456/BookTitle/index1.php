<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Pagination</h2>
    <p>The pagination class provides pagination links:</p>
    <ul class="pagination">
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
    </ul>
</div>

</body>
</html>


